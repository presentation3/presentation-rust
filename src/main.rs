fn main() {

}

fn calc(a: i32, b: i32) -> i32 {
    a + b
}

#[cfg(test)]
mod tests {
    use crate::calc;

    #[test]
    fn test_calc() {
        assert_eq!(calc(2, 3), 5);
    }

    #[test]
    fn test_calc2() {
        assert_eq!(calc(8, 3), 11);
    }
}